## Requirements

- [Rust](https://www.rust-lang.org/en-US/install.html)
- [Redis](https://redis.io/) or [Docker Compose](https://docs.docker.com/compose/)


## Installation

Run `cargo install` for installing project dependencies.

Have a running instance or Redis on 127.0.0.1:6379, or run `docker-compose up -d`.


## Running in development

    cargo run


Any package that is sent by UDP on 127.0.0.1:5005 will be saved into Redis.
