extern crate redis;
extern crate time;

use time::precise_time_ns;
use std::str;
use std::net::UdpSocket;
use redis::Commands;


fn write_redis(con: &redis::Connection, key: &str, value: &str) -> redis::RedisResult<()> {
    let _: () = try!(con.set(key, value));
    Ok(())
}


fn connect_redis() -> redis::RedisResult<(redis::Connection)> {
    // general connection handling
    let client = try!(redis::Client::open("redis://127.0.0.1:6379/"));
    let con = try!(client.get_connection());
    Ok((con))
}


fn main() {
    let address: &'static str = "0.0.0.0:5005";
    let con = connect_redis().unwrap();

    let socket = match UdpSocket::bind(address) {
        Ok(s) => s,
        Err(e) => panic!("couldn't bind socket: {}", e)
    };
    println!("Listening on {}", address);

    let mut buf = [0; 2048];
    loop {
        match socket.recv_from(&mut buf) {
            Ok((_, src)) => {
                let now = precise_time_ns();
                let ip = &format!("{}-{}", now, src).replace(":", ",");
                write_redis(&con, ip, str::from_utf8(&buf).unwrap_or(""));
            },
            Err(e) => {
                println!("couldn't read package {}", e);
            }
        }
    }
}
